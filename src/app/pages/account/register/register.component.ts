import { Component, OnInit } from '@angular/core';
import {Register} from './model';
import { WebService } from 'src/app/http/web-service.service';
import {Url} from '../../../http/url-constants';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  public registerObj= new Register();
  constructor(private webService:WebService) { }

  ngOnInit(): void {
  }

  signUp() {
    delete this.registerObj.dateOfBirth;
    this.webService.post(Url.API.user.signup,this.registerObj).subscribe((response) => {
        this.registerObj=new Register();
        if (response.status && response.status.code === 200) {
        }
      }, () => {
        
      });
  }
  
}
