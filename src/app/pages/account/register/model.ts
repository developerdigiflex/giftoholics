export interface Address{
    id:string;
    userId:string;
    alias:string;
    isPrimary:boolean;
    streetAddress1:string;
    streetAddress2:string;
    city:string;
    state:string;
    country:string;
    pin:string;
}

export class Register{
    firstName:string;
    lastName:string;
    emailId:string;
    mobileNo:string;
    countryCode:string;
    gender:string;
    dateOfBirth:Date;
    password:string;
    address:Address;
}