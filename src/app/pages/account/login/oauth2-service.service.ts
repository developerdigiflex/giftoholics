import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Url} from '../../../http/url-constants';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class Oauth2ServiceService {
  
  constructor(private http: HttpClient, private cookieService: CookieService) {
  }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : Login for user
   * @param :
   * @return {void}
   */
  public login(object): Observable<any> {    
    return this.http.post(Url.API.user.login,object);
  }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : login for home page user {application specific)
   * @param :
   * @return {void}
   */
  // public loginForHome(): any {
  //   const loginObj = new OAuth2RequestProto();
  //   loginObj.authType = 'apiclient';
  //   loginObj.password = 'VAPAPP';
  //   loginObj.realm = 'Citizen';
  //   loginObj.username = 'VAP';
  //   return this.http.post(Url.API.user.login , this.formatData(loginObj), this.getRequestOptions());
  // }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : Refresh token
   * @param :
   * @return {void}
   */
  // public refreshToken(): Observable<any> {
  //   this.cookieService.removeCookie('fgsacthfdsg');
  //   const object = new OAuth2RequestProto();
  //   object.grant_type = 'refresh_token';
  //   object.refresh_token = this.cookieService.getCookie('gdsrefshdsf');
  //   return this.http.post(Url.CAP_URL + Url.API.Authentication.loginToken, this.formatData(object), this.getRequestOptions());
  // }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : storing token related information in cookie
   * @param : access_token
   * @param : expires_in
   * @param : access_token
   * @return {void}
   */
  public storeAccessTokenResponse(access_token: string, expires_in: string, refresh_token: string): void {
    /*if (expires_in) {
     const expiresInMilliSeconds = expires_in * 1000;
     let now = new Date();
     let expiresAt = now.getTime() + expiresInMilliSeconds;
     this.cookieService.setCookie('fgskexphfdsg', "" + expiresAt);
     } else {
     let expireDate = new Date();
     expireDate.setDate(expireDate.getDate() + 365);
     this.cookieService.setCookie('fgskexphfdsg', "" + expireDate);
     }*/
   // this.cookieService.setCookie('fgsacthfdsg', access_token);
    //this.cookieService.setCookie('gdsrefshdsf', refresh_token);
  }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : check that is user logged in or not
   * @return {void}
   */
  public isAuthenticated(): boolean {
    if (this.cookieService.get('fdgsgfjs')) {
      return true;
    }
    return false;
  }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : get access token from cookie
   * @param :
   * @return {void}
   */
  // public getAccessToken(): string {
  //  // return this.cookieService.getCookie('fgsacthfdsg');
  // }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : get refresh token
   * @param :
   * @return {void}
   */
  // public getRefreshToken(): string {
  //   //return this.cookieService.getCookie('gdsrefshdsf');
  // }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : set user is logged in.
   * @param :
   * @return {void}
   */
  public setUserLoggedIn(): void {
    this.cookieService.put('fdgsgfjs', 'Smehhen');
  }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : logout for removing all the details about token or logged in
   * @param :
   * @return {void}
   */
  // public logoutSystem(): Observable<any> {
  //   // const httpOptions: any = {};
  //   // /*set header section*/
  //   // const httpHeader = {};
  //   // httpHeader['language'] = 'en';
  //   // httpHeader['Authorization'] = 'Bearer ' + this.getAccessToken();
  //   // httpOptions['headers'] = new HttpHeaders(httpHeader);
  //   // return this.http.post(Url.CAP_URL + Url.API.Authentication.logout, {
  //   //   token: this.getAccessToken()
  //   // }, httpOptions);
  // }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : logout for removing all the details about token or logged in
   * @param :
   * @return {void}
   */
  public OAuthLogout(): boolean {
    // this.cookieService.removeCookie('sjgnei');
    // this.cookieService.removeCookie('fgsacthfdsg');
    // this.cookieService.removeCookie('gdsrefshdsf');
    this.cookieService.remove('fdgsgfjs');
    this.cookieService.removeAll();
    return true;
  }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : getting all request header option for service
   * @param :
   * @return {RequestOptionsArgs}
   */
  private getRequestOptions(): any {
    const httpOptions = new Object();
    /*set header section*/
    const httpHeader = {};
    httpHeader['Content-Type'] = 'application/x-www-form-urlencoded';
    httpHeader['Authorization'] = 'Basic cmVzdGFwcDpyZXN0YXBw';
    httpOptions['headers'] = new HttpHeaders(httpHeader);
    return httpOptions;
  }

  /**
   * @author: Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : format data in query string url
   * @param :
   * @return {string}
   */
  public formatData(data: any): string {
    let returnData = '';
    let count = 0;
    for (const i in data) {
      if (count === 0) {
        returnData += i + '=' + data[i];
      } else {
        returnData += '&' + i + '=' + data[i];
      }
      count = count + 1;
    }
    return returnData;
  }

}
