import { Component, OnInit } from '@angular/core';
import {loginObj} from './model';
import {Oauth2ServiceService} from './oauth2-service.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public login = new loginObj();
  constructor(private oAuthService: Oauth2ServiceService) { }

  ngOnInit(): void {
  }

  onSubmit(loginData: loginObj) {
    this.oAuthService.login(loginData).subscribe((response) => {
     
    }, (error) => {
      
    });
  }

}
