import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewportScroller } from '@angular/common';
import { ProductService } from "../../../shared/services/product.service";
import { Product } from '../../../shared/classes/product';
import { WebService } from 'src/app/http/web-service.service';
import {ProductFilter} from '../model';
import {Url} from '../../../http/url-constants';
import { Options } from 'ng5-slider';
@Component({
  selector: 'app-collection-left-sidebar',
  templateUrl: './collection-left-sidebar.component.html',
  styleUrls: ['./collection-left-sidebar.component.scss']
})
export class CollectionLeftSidebarComponent implements OnInit {
  
  public grid: string = 'col-xl-3 col-md-6';
  public layoutView: string = 'grid-view';
  public products: any[] = [];
  public brands: any[] = [];
  public categories: any[] = [];
  public subCategories: any[] = [];
  public showSub=false;
  public colors: any[] = [];
  public size: any[] = [];
  public minPrice: number = 0;
  public maxPrice: number = 12000;

  options: Options = {
    floor: 0,
    ceil: 12000
  };
  options1: Options = {
    floor: 0,
    ceil: 100
  };
  public tags: any[] = [];
  public category: string;
  public pageNo: number = 1;
  public paginate: any = {}; // Pagination use only
  public sortBy: string; // Sorting Order
  public mobileSidebar: boolean = false;
  public loader: boolean = true;
  public productObj=new ProductFilter();
  constructor(private route: ActivatedRoute, private router: Router,
    private viewScroller: ViewportScroller, public productService: ProductService,private webService:WebService) {   
      // Get Query params..
      this.route.queryParams.subscribe(params => {
        this.productObj.categoryId=[];
          this.productObj.categoryId.push(params.c);
          this.productObj.subCategoryId=[];
          if(params.s!=undefined){
            
            this.productObj.subCategoryId.push(params.s);
            this.showSub=true;

          }
          this.productObj.minTotalPrice=1200;
          this.productObj.maxTotalPrice=3000;
          this.productObj.minDiscountPercentage=0;
          this.productObj.maxDiscountPercentage=10;
          this.getAllCategory();
          this.getProduct();
        // this.brands = params.brand ? params.brand.split(",") : [];
        // this.colors = params.color ? params.color.split(",") : [];
        // this.size  = params.size ? params.size.split(",")  : [];
        // this.minPrice = params.minPrice ? params.minPrice : this.minPrice;
        // this.maxPrice = params.maxPrice ? params.maxPrice : this.maxPrice;
        // this.tags = [...this.brands, ...this.colors, ...this.size];
        
        // this.category = params.category ? params.category : null;
        // this.sortBy = params.sortBy ? params.sortBy : 'ascending';
        // this.pageNo = params.page ? params.page : this.pageNo;

        // Get Filtered Products..
        // this.productService.filterProducts(this.tags).subscribe(response => {         
        //   // Sorting Filter
        //   this.products = this.productService.sortProducts(response, this.sortBy);
        //   // Category Filter
        //   if(params.category)
        //     this.products = this.products.filter(item => item.type == this.category);
        //   // Price Filter
        //   this.products = this.products.filter(item => item.price >= this.minPrice && item.price <= this.maxPrice) 
        //   // Paginate Products
        //   this.paginate = this.productService.getPager(this.products.length, +this.pageNo);     // get paginate object from service
        //   this.products = this.products.slice(this.paginate.startIndex, this.paginate.endIndex + 1); // get current page of items
        // })
      })
  }

  ngOnInit(): void {
   
    
    
  }
  getAllCategory(){
    this.webService.get(Url.API.category.get).subscribe((response) => {
      this.categories=response;
      if (response.status && response.status.code === 200) {
        //this.categories= 

      }
    }, () => {
      
    });
  }

  getSubCategoryById(id){
    this.productObj.categoryId[0]=id;
    this.webService.get(Url.API.subcategory.getbyId+id).subscribe((response) => {
      this.subCategories=response;
      if(this.subCategories.length>0){
        this.productObj.subCategoryId=[];
        this.showSub=true;
      }else{
        this.showSub=false;
      }
      this.getProduct();
      if (response.status && response.status.code === 200) {
        //this.categories= 
      }
    }, () => {
      
    });
  }

  appliedFilter(event) {
    let index = this.subCategories.findIndex(x => x.id ===event.target.value);  // checked and unchecked value
    if (event.target.checked)   {
      this.productObj.subCategoryId.push(event.target.value); // push in array cheked value
    }
    else {
    this.productObj.subCategoryId.splice(index,1);  // removed in array unchecked value  
    }
    this.getProduct();
    // let brands = this.brands.length ? { brand: this.brands.join(",") } : { brand: null };
    // this.brandsFilter.emit(brands);
  }

  // check if the item are selected
  checked(item){
    if(this.productObj.subCategoryId.length>0){
      if(this.productObj.subCategoryId.indexOf(item) != -1){
        return true;
      }
    }
  }

  getProduct(){
    this.webService.post(Url.API.product.getProducts,this.productObj).subscribe((response) => {
        this.products=response.products;
      if (response.status && response.status.code === 200) {
        

      }
    }, () => {
      
    });
  }

  // Append filter value to Url
  updateFilter(tags: any) {
    this.productObj.minTotalPrice=tags.value;
    this.productObj.maxTotalPrice=tags.highValue;
    tags.page = null; // Reset Pagination
    // this.router.navigate([], { 
    //   relativeTo: this.route,
    //   queryParams: tags,
    //   queryParamsHandling: 'merge', // preserve the existing query params in the route
    //   skipLocationChange: false  // do trigger navigation
    // }).finally(() => {
    //   this.viewScroller.setOffset([120, 120]);
    //   this.viewScroller.scrollToAnchor('products'); // Anchore Link
    // });
  }

  // SortBy Filter
  sortByFilter(value) {
    this.router.navigate([], { 
      relativeTo: this.route,
      queryParams: { sortBy: value ? value : null},
      queryParamsHandling: 'merge', // preserve the existing query params in the route
      skipLocationChange: false  // do trigger navigation
    }).finally(() => {
      this.viewScroller.setOffset([120, 120]);
      this.viewScroller.scrollToAnchor('products'); // Anchore Link
    });
  }

  // Remove Tag
  removeTag(tag) {
  
    this.brands = this.brands.filter(val => val !== tag);
    this.colors = this.colors.filter(val => val !== tag);
    this.size = this.size.filter(val => val !== tag );

    let params = { 
      brand: this.brands.length ? this.brands.join(",") : null, 
      color: this.colors.length ? this.colors.join(",") : null, 
      size: this.size.length ? this.size.join(",") : null
    }

    this.router.navigate([], { 
      relativeTo: this.route,
      queryParams: params,
      queryParamsHandling: 'merge', // preserve the existing query params in the route
      skipLocationChange: false  // do trigger navigation
    }).finally(() => {
      this.viewScroller.setOffset([120, 120]);
      this.viewScroller.scrollToAnchor('products'); // Anchore Link
    });
  }

  // Clear Tags
  removeAllTags() {
    this.router.navigate([], { 
      relativeTo: this.route,
      queryParams: {},
      skipLocationChange: false  // do trigger navigation
    }).finally(() => {
      this.viewScroller.setOffset([120, 120]);
      this.viewScroller.scrollToAnchor('products'); // Anchore Link
    });
  }

  // product Pagination
  setPage(page: number) {
    this.router.navigate([], { 
      relativeTo: this.route,
      queryParams: { page: page },
      queryParamsHandling: 'merge', // preserve the existing query params in the route
      skipLocationChange: false  // do trigger navigation
    }).finally(() => {
      this.viewScroller.setOffset([120, 120]);
      this.viewScroller.scrollToAnchor('products'); // Anchore Link
    });
  }

  // Change Grid Layout
  updateGridLayout(value: string) {
    this.grid = value;
  }

  // Change Layout View
  updateLayoutView(value: string) {
    this.layoutView = value;
    if(value == 'list-view')
      this.grid = 'col-lg-12';
    else
      this.grid = 'col-xl-3 col-md-6';
  }

  // Mobile sidebar
  toggleMobileSidebar() {
    this.mobileSidebar = !this.mobileSidebar;
  }

}
