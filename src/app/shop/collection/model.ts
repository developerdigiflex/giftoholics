export class ProductFilter{
    categoryId:string[];
    subCategoryId:string[];
    minTotalPrice:number;
    maxTotalPrice:number;
    availiability:number;
    minDiscountPercentage:number; 
    maxDiscountPercentage:number;
    vendorId:string[];
    canShipGlobally:boolean;
    tags:string[];
    pageNumber:number;
    pageSize:number;
    searchText:string;
  }