/**
 * Created by mayur.
 */

export class Url {
  public static serverUrl = 'http://ec2-13-235-13-125.ap-south-1.compute.amazonaws.com:8582';
  public static API = {
    'user':{
      'login':'/user/login',
      'signup':'/user/sign-up'
    },
    'home':{
      'getHome':'/home-page/get'
    },
    'product': {
      'getCatgories': '/sub-category/getAllCategoriesAndSubCategories',
      'getProducts': '/product/get/filter/all-Products'
    },
    'category':{
      'add': '/category/add',
      'get': '/category/getAll',
      'getCatgories': '/sub-category/getAllCategoriesAndSubCategories'
    },
    'subcategory':{
      'add': '/sub-category/add',
      'get': '/sub-category/getAll',
      'getbyId':'/sub-category/getByCategory/',
    }

  };
}
