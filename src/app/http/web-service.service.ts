import {Injectable, Optional} from '@angular/core';
import {WebServiceConfig} from './webserviceconfig';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Url} from './url-constants';
import { timer } from 'rxjs';
import {Subject} from 'rxjs';
//import {Oauth2ServiceService} from '../featureModule/authentication/oauth2-service.service';
//import {LogoutService} from '../featureModule/authentication/logout/logout.service';

@Injectable()
export class WebService {

  public unAuthorizedError: boolean = false;
  public fileUploadProgress: Subject<any> = new Subject<any>();

  constructor(@Optional() private  config: WebServiceConfig,
              private http: HttpClient) {
  }


  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : get token from cookie
   * @param :
   * @returns: {string}
   */
    private getTokenDetails() {
    //return this.oAuthService.getAccessToken();
    }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : check token if token get will return else continue 10 sec and return
   * @param :
   * @returns: {void}
   */
  private checkToken(): Promise<any> {
    return new Promise((resolve) => {
      let count = 0;
      const source = timer(1000, 1000);
      const subscription = source.subscribe(time => {
        count++;
        if (count > 5) {
          subscription.unsubscribe();
          resolve(true);
        }
        // if (this.oAuthService.getAccessToken()) {
        //   subscription.unsubscribe();
        //   resolve(true);
        // }
      });
    });
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description :Build base API url.
   * @param : url
   * @param : backendUrl
   * @returns: {string}
   */
  private getFullUrl(url: string, backendUrl?: string): string {
    if (backendUrl) {
      return backendUrl + url;
    }
    return Url.serverUrl + url;
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description :get RequestOptions.
   * @param : [params]
   * @param : [body]
   * @returns: {any}
   */
  private getRequestOptions(params?: any, body?: any, contentType?: string): any {
    const httpOptions: any = {};

    /*set header section*/
    const httpHeader = {};
    httpHeader['Authorization'] = 'Bearer ' + this.getTokenDetails();
    httpHeader['language'] = 'en';

    /*set content type for multipart*/
    if (contentType) {
      httpHeader['Content-type'] = contentType;
    }
    httpOptions['headers'] = new HttpHeaders(httpHeader);

    /*Set param section*/
    if (params) {
      httpOptions['params'] = new HttpParams({fromObject: params});
    }

    /*Set param section*/
    if (body) {
      httpOptions['body'] = body;
    }
    return httpOptions;
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : handle http errors, if invalid token or ( unAutorized)401 error occure then it will call refresh token
   * @param : error
   * @returns: {void}
   */
  private handleError(error: HttpErrorResponse | any) {
    // return new Observable((resolve) => {
    //   if (error.status === 401) {
    //     if (this.unAuthorizedError) {
    //       resolve.next('unAuthorizedError');
    //     } else {
    //       this.unAuthorizedError = true;
    //       this.oAuthService.refreshToken()
    //         .subscribe((response) => {
    //           this.oAuthService.storeAccessTokenResponse(response.access_token, response.expires_in, response.refresh_token);
    //           this.unAuthorizedError = false;
    //           resolve.next('success');
    //         }, (tokenError) => {
    //           this.logoutService.logout();
    //           this.unAuthorizedError = false;
    //           resolve.next('error');
    //         });
    //     }
    //   } else {
    //     resolve.next('error');
    //   }
    // });
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : http get method
   * @param :endPoint
   * @param :[backendUrl]
   * @returns: {Observable<R>}
   */
  public get(endPoint: string, backendUrl?: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {

        this.http.get(this.getFullUrl(endPoint, backendUrl), this.getRequestOptions())
          .subscribe(response => resolve.next(response), (httpError) => {
            count++;
            
          });

      };

      takeCallback();
    });
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : http get method with params
   * @param :endPoint
   * @param :params
   * @param :[backendUrl]
   * @returns: {Observable<R>}
   */
  public getWithParams(endPoint: string, params: any, backendUrl?: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {

        this.http.get(this.getFullUrl(endPoint, backendUrl), this.getRequestOptions(params))
          .subscribe(response => resolve.next(response), (httpError) => {
            count++;
            
          });

      };

      takeCallback();
    });
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : http get method with params
   * @param :endPoint
   * @param :params
   * @param :[backendUrl]
   * @returns: {Observable<R>}
   */
  public getFromUrl(endPoint: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {

        this.http.get(endPoint, this.getRequestOptions())
          .subscribe(response => resolve.next(response), (httpError) => {
            count++;
          
          });

      };

      takeCallback();
    });
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : http get method with params
   * @param :endPoint
   * @param :params
   * @param :[backendUrl]
   * @returns: {Observable<R>}
   */
  public getFromUrlWithOutHeader(endPoint: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {

        this.http.get(endPoint)
          .subscribe(response => resolve.next(response), (httpError) => {
            count++;
            
          });

      };

      takeCallback();
    });
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : http post method
   * @param :endPoint
   * @param :data
   * @param :[backendUrl]
   * @returns: {Observable<R>}
   */
  public post(endPoint: string, data: any, backendUrl?: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {

        this.http.post(this.getFullUrl(endPoint, backendUrl), data, this.getRequestOptions())
          .subscribe(response => resolve.next(response), (httpError) => {
           
          });

      };

      takeCallback();
    });
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : http post from url method
   * @param :endPoint
   * @param :data
   * @param :[backendUrl]
   * @returns: {Observable<R>}
   */
  public postFromUrl(endPoint: string, data: any): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {

        this.http.post(endPoint, data, this.getRequestOptions())
          .subscribe(response => resolve.next(response), (httpError) => {
            count++;
            
          });

      };

      takeCallback();
    });
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : http put method
   * @param :endPoint
   * @param :data
   * @param :params
   * @param :[backendUrl]
   * @returns: {Observable<R>}
   */
  public put(endPoint: string, data?: any, params?: any, backendUrl?: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {

        this.http.put(this.getFullUrl(endPoint, backendUrl), data, this.getRequestOptions(params))
          .subscribe(response => resolve.next(response), (httpError) => {
            count++;
           
          });

      };

      takeCallback();
    });
  }

  /***
   * @Author : Ganeshram Kumhar (ganeshram.kumhar@kahunasystems.com)
   * @description : http delete method
   * @param :endPoint
   * @param :data
   * @param :params
   * @param :[backendUrl]
   * @returns: {Observable<R>}
   */
  public delete(endPoint: string, data?: any, backendUrl?: string, param?: any): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {

        this.http.delete(this.getFullUrl(endPoint, backendUrl), this.getRequestOptions(param, data))
          .subscribe(response => resolve.next(response), (httpError) => {
            count++;
           
          });

      };

      takeCallback();
    });
  }

  /**
   * @author: Dinesh shah (dinesh.shah@kahunasystems.com)
   * @description : multipart file upload with progress event.
   * @param endPoint
   * @param formData
   * @param backendUrl
   * @return {observer}
   */
  multipartFileUpload(endPoint: string, formData: FormData, data?: any, backendUrl?: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {
        let xhr: XMLHttpRequest;

        const requestOptions = this.getRequestOptions();
        return Observable.create((observer: any) => {
          xhr = new XMLHttpRequest();
          xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
              if (xhr.status === 200) {
                observer.next(JSON.parse(xhr.response));
                observer.complete();
              } else {
                observer.error(xhr.response);
              }
            }
          };
          xhr.upload.onprogress = (event) => {
            const progress = Math.round(event.loaded / event.total * 100);
            this.fileUploadProgress.next(progress);
          };
          xhr.open('POST', this.getFullUrl(endPoint, backendUrl), true);
      //    xhr.setRequestHeader('Authorization', requestOptions.headers.get('Authorization'));
        //  xhr.setRequestHeader('language', requestOptions.headers.get('language'));
          xhr.send(formData);
        }).subscribe(response => resolve.next(response), (httpError) => {
          count++;
        });

      };

      takeCallback();
    });
  }

  /**
   * @author: Dinesh shah (dinesh.shah@kahunasystems.com)
   * @description : multipart file upload with progress event.
   * @param endPoint
   * @param formData
   * @param backendUrl
   * @return {observer}
   */
  multipartFileUploadForPUT(endPoint: string, formData: FormData, data?: any, backendUrl?: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {
        let xhr: XMLHttpRequest;

        const requestOptions = this.getRequestOptions();
        return Observable.create((observer: any) => {
          xhr = new XMLHttpRequest();
          xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
              if (xhr.status === 200) {
                observer.next(JSON.parse(xhr.response));
                observer.complete();
              } else {
                observer.error(xhr.response);
              }
            }
          };
          xhr.upload.onprogress = (event) => {
            const progress = Math.round(event.loaded / event.total * 100);
            this.fileUploadProgress.next(progress);
          };
          xhr.open('PUT', this.getFullUrl(endPoint, backendUrl), true);
          xhr.setRequestHeader('Authorization', requestOptions.headers.get('Authorization'));
          xhr.setRequestHeader('language', requestOptions.headers.get('language'));
          xhr.send(formData);
        }).subscribe(response => resolve.next(response), (httpError) => {
          count++;
          
        });

      };

      takeCallback();
    });
  }

  public downloadCertificate(endPoint: string, contentType: string, backendUrl?: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {
        const requestOptions = this.getRequestOptions();
        requestOptions.responseType = 'blob';
        this.http.get(this.getFullUrl(endPoint, backendUrl), requestOptions)
          .subscribe(response => resolve.next(response), (httpError) => {
            count++;
           
          });

      };

      takeCallback();
    });
  }

  public downloadCertificatePost(endPoint: string, data: any, contentType: string, backendUrl?: string): Observable<any> {
    let count = 0;
    return new Observable((resolve) => {
      const takeCallback = (): void => {
        const requestOptions = this.getRequestOptions();
        requestOptions.responseType = 'blob';
        this.http.post(this.getFullUrl(endPoint, backendUrl), data, requestOptions)
          .subscribe(response => resolve.next(response), (httpError) => {
            count++;
           
          });

      };

      takeCallback();
    });
  }
}

