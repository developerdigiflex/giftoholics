import { Component, OnInit } from '@angular/core';
import { NavService, Menu } from '../../services/nav.service';
import { Router } from '@angular/router';
import {WebService} from '../../../http/web-service.service';
import {Url} from '../../../http/url-constants';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public menuItems: Menu[];
  public categories:any[]=[];
  constructor(private router: Router, public navServices: NavService, private webService:WebService) {
    this.navServices.items.subscribe(menuItems => this.menuItems = menuItems );
    this.router.events.subscribe((event) => {
      this.navServices.mainMenuToggle = false;
    });
  }

  ngOnInit(): void {
    this.getAllCategory();
  }

  hasSubCategory(child){
    if(child.subCategories.length>0){
      return true;
    }else{
      return false;
    }
  }

  getAllCategory(){
    this.webService.get(Url.API.category.getCatgories).subscribe((response) => {
      this.categories=[];
      for(let i=0;i<response.length;i++){
        if(i<5){
          this.categories.push(response[i]);
        }
      }
      
      if (response.status && response.status.code === 200) {
      }
    }, () => {
      
    });
  }

  mainMenuToggle(): void {
    this.navServices.mainMenuToggle = !this.navServices.mainMenuToggle;
  }

  // Click Toggle menu (Mobile)
  toggletNavActive(item) {
    item.active = !item.active;
  }

}
