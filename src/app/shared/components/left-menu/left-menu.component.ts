import { Component, OnInit } from '@angular/core';
import { NavService, Menu } from '../../services/nav.service';
import { Router } from '@angular/router';
import {WebService} from '../../../http/web-service.service';
import {Url} from '../../../http/url-constants';
import {ProductService} from '../../services/product.service'
@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {

  public menuItems: Menu[];
  public categories:any=[];
  constructor(private router: Router, public navServices: NavService,private webService:WebService,private productService:ProductService) {
    this.navServices.leftMenuItems.subscribe(menuItems => this.menuItems = menuItems );
    this.router.events.subscribe((event) => {
      this.navServices.mainMenuToggle = false;
    });
  }

  ngOnInit(): void {
    this.getAllCategory();
  }

  hasSubCategory(child){
    if(child.subCategories.length>0){
      return true;
    }else{
      return false;
    }
  }

  leftMenuToggle(): void {
    this.navServices.leftMenuToggle = !this.navServices.leftMenuToggle;
  }

  // Click Toggle menu (Mobile)
  toggletNavActive(item) {
    item.active = !item.active;
  }

  changeRoute(name,cId,sId,dec){
    
    if(dec){
      this.router.navigate(['/shop/collection/left/sidebar'], { queryParams: { category: name,
        c: encodeURI(cId) 
        } });
    }else{
      this.router.navigate(['/shop/collection/left/sidebar'], { queryParams: { category: name,
        c: encodeURI(cId),
        s: encodeURI(sId) 
        } });
    }
    
  }

  getAllCategory(){
    this.webService.get(Url.API.category.getCatgories).subscribe((response) => {
      this.categories=response;
      if (response.status && response.status.code === 200) {
        //this.categories= 

      }
    }, () => {
      
    });
  }

}
