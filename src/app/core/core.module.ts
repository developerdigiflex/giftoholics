import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserService } from './user-service/user.service';
import { DataStoreService } from './data-store/data-store.service';


@NgModule({
    declarations: [],
    exports: [
        BrowserModule,
        BrowserAnimationsModule
    ],
    providers: [UserService, DataStoreService]
})
export class CoreModule {
}
