import { HttpServiceConstants } from '@kimaya/ngx-http-service';
import { AuthServiceConstants } from '@kimaya/ngx-auth-service';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

export class Utility {
    public static getDialogData(data?: any,
                                size: 'sm' | 'lg' | 'xl' = 'lg',
                                centered: boolean = true,
                                backdrop: boolean | 'static' = true,
                                ignoreBackdropClick: boolean = false,
                                animated: boolean = true): NgbModalConfig {
        const config = {
            data,
            backdrop,
            ignoreBackdropClick,
            animated,
            centered,
        } as any;
        switch (size) {
            case 'lg':
                config.size = 'lg';
                break;
            case 'xl':
                config.size = 'xl';
                break;
            default:
                config.size = 'sm';
                break;
        }
        return config;
    }

    public static getDeleteMessage(data?: any): any {
        return {
            data: {
                title: data && data.title ? data.title : 'CONSTANTS.DELETE.TITLE',
                message: data && data.message ? data.message : 'CONSTANTS.DELETE.MESSAGE'
            }, size: 'sm'
        };
    }

    public static scrollTo(attr: string): void {
        setTimeout(() => {
            const elementList = document.querySelectorAll(attr);
            if (elementList && elementList[0]) {
                const element = elementList[0] as HTMLElement;
                /* element.scrollIntoView({behavior: 'smooth', block: 'start'});
                 element.scrollTop += 120;*/

                let headerOffset = 140;
                let elementPosition = element.getBoundingClientRect().top;
                let offsetPosition = elementPosition - headerOffset;

                window.scrollTo({
                    top: offsetPosition,
                    behavior: 'smooth'
                });
            }
        }, 0);
    }

    public static setHeaders(key: string, value: string): void {
        if (value) {
            if (HttpServiceConstants.headers) {
                HttpServiceConstants.headers[key] = value;
            }
            if (AuthServiceConstants.headers) {
                AuthServiceConstants.headers[key] = value;
            }
        } else {
            delete AuthServiceConstants.headers[key];
        }
    }

    public static removeHeaders(key: string): void {
        if (HttpServiceConstants.headers) {
            delete HttpServiceConstants.headers[key];
        }
        if (AuthServiceConstants.headers) {
            delete AuthServiceConstants.headers[key];
        }
    }

    public static getRedirectionUrl(urlPart: string): string {
        let paths = window.location.pathname.split('/');
        if (paths.length > 0) {
            paths[paths.length - 2] = urlPart;
            //    console.log(window.origin + paths.join('/'));
            return window.origin + paths.join('/');
        } else {
            return window.origin + +'/' + urlPart;
        }

    }

    public static sortList(list: any[], attr: string, order: 'asc' | 'desc' = 'asc'): any[] {
        let orderNumber = 1;
        switch (order) {
            case 'asc':
                orderNumber = 1;
                break;
            case 'desc':
                orderNumber = -1;
                break;
        }
        return list.sort((data1, data2) => {
            const value1 = data1[attr];
            const value2 = data2[attr];
            let result = null;

            if (value1 == null && value2 != null) {
                result = -1;
            } else if (value1 != null && value2 == null) {
                result = 1;
            } else if (value1 == null && value2 == null) {
                result = 0;
            } else if (typeof value1 === 'string' && typeof value2 === 'string') {
                result = value1.localeCompare(value2);
            } else {
                result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
            }

            return (orderNumber * result);
        });
    }

    public static findObject(list, attr, matchedValue): any {
        return list.find(result => result[attr] === matchedValue.toString());
    }

    public static getMultiSelectArrayList(data, field = '', value?: any) {
        const temp = [];
        if (data && data.length > 0) {
            if (field === '') {
                data.forEach((option) => {
                    temp.push({'value': option, 'label': option});
                });
            } else {
                data.forEach((option) => {
                    temp.push({'value': option[value], 'label': option[field]});
                });
            }
        }
        return temp;
    }

    public static generateUUID(): string {
        const stringArr = [];
        for (let i = 0; i < 5; i++) {
            // tslint:disable-next-line:no-bitwise
            const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            stringArr.push(S4);
        }
        return stringArr.join('-');
    }

    public static getTextColor(color: string): string {
        try {
            if (color) {
                const rgb = this.getRGB(color);
                const o = Math.round((rgb[0] * 299 + rgb[1] * 587 + rgb[2] * 114) / 1000);
                return o > 125 ? 'black' : 'white';
            }
        } catch (e) {
            console.error(e);
        }
    }

    public static daysToMilliseconds(days) {
        return days * 24 * 60 * 60 * 1000;
    }

    public static copyToClipboard(val: string): void {
        const selBox = document.createElement('textarea');
        // pattern for br tag
        var brRegex = /<br\s*[\/]?>/gi;
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        // Replace br tag by \r\n for new line
        selBox.textContent = val.replace(brRegex, '\r\n');
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
    }

    private static getRGB(color: string, data?: any): number[] {
        if (data) {
            return [data[0], data[1], data[2]];
        }
        try {
            const hex = color.replace('#', '');
            const r = parseInt(hex.slice(0, 2), 16);
            const g = parseInt(hex.slice(2, 4), 16);
            const b = parseInt(hex.slice(4, 6), 16);

            return [r, g, b];
        } catch (e) {
            console.error(e);
        }
    }
}

export interface UtilReturnFunctionType {
    isEmpty(): boolean;

    hasValue(): boolean;
}

export declare function util(param: any): UtilReturnFunctionType;
