import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DataStoreService {

    public APP_SETTINGS: Map<string, any> = new Map();

    constructor() {
    }

}
