import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { DataStoreService } from '../data-store/data-store.service';


export class UserProfilePojo {
    profile: string = 'assets/images/user/user-profile.png';
    firstName: string = 'Giftoholics';
    lastName: string = 'User';
    roles: string[];
    organisations: TeamModal[];
    emailId: string;
    phoneNumber?: string;
    crmId: string;
}

export class TeamModal {
    id: string;
    name: string;
    alias: string;
    email: string;
    phone: string;
}

@Injectable({
    providedIn: 'root'
})
export class UserService {

    public Observable: ReplaySubject<UserProfilePojo> = new ReplaySubject();
    private userData: UserProfilePojo = new UserProfilePojo();

    constructor() {
        this.Observable.next(new UserProfilePojo());
    }

    setUser(data: UserProfilePojo): void {
        //    CoreConstants.FIRST_PAGE = '/dashboard';
        if (!data.organisations) {
            data.organisations = [];
        }
        if (data) {
            const keys = Object.keys(data);
            keys.forEach((key) => {
                this.userData[key] = data[key];
            });
        }
        this.Observable.next(this.userData);

    }

    setProjectPermission(): void {
        this.Observable.next(this.userData);
    }


    getObserver(): Observable<UserProfilePojo> {
        return this.Observable;
    }

    getUser(): UserProfilePojo {
        return this.userData;
    }

    setUserProperty(key: string, value: any): void {
        this.userData[key] = value;
        this.Observable.next(this.userData);
    }

    getUserProperty(key: string): any {
        return this.userData[key];
    }

    resetUser(): void {
        this.userData = new UserProfilePojo();
    }

}
