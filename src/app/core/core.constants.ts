import { BehaviorSubject } from 'rxjs';

export class CoreConstants {
    public static HOMEPAGE = '/dashboard';

    constructor() {
    }
}


export interface DefaultStatusResponse {
    code: number;
    message: string;
    cause: string;
}
