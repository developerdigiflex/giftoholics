import { Component, OnInit } from '@angular/core';
import { ProductSlider } from '../../../shared/data/slider';
import { Product } from '../../../shared/classes/product';
import { ProductService } from '../../../shared/services/product.service';
import { WebService } from 'src/app/http/web-service.service';
import {Url} from '../../../http/url-constants';
import {HomePageResponse} from '../model';
import { Router } from '@angular/router';
import { BlogSlider } from '../../../shared/data/slider';
@Component({
  selector: 'app-fashion-one',
  templateUrl: './fashion-one.component.html',
  styleUrls: ['./fashion-one.component.scss']
})
export class FashionOneComponent implements OnInit {

  public products: Product[] = [];
  public productCollections: any[] = [];
  public homepageObj = new HomePageResponse();
  public festive:any[]=[];
  public BlogSliderConfig: any = BlogSlider;
  public newA:any[]=[];
  public bestseller:any[]=[];
  constructor(public productService: ProductService,private webService:WebService,public router:Router) {
    this.getHome();
    this.productService.getProducts.subscribe(response => {
      this.products = response.filter(item => item.type == 'fashion');
      // Get Product Collection
      this.products.filter((item) => {
        item.collection.filter((collection) => {
          const index = this.productCollections.indexOf(collection);
          if (index === -1) this.productCollections.push(collection);
        })
      })
    });
  }

  public ProductSliderConfig: any = ProductSlider;
  //1920 x 718
  public sliders = [
    
  ];

  // Collection banner 300 x 300
  public services = [];
  public trendShop=[
    {
      image:'assets/images/momtobe.png',
      text:'Mom to be'
    },
    {
      image:'assets/images/bridalshower.png',
      text:'Bridal Shower'
    },
    {
      image:'assets/images/ohbaby.png',
      text:'Oh baby'
    },
    {
      image:'assets/images/weddinggift.png',
      text:'Wedding gifts'
    },
    {
      image:'assets/images/weddinggiveaways.png',
      text:'Wedding give aways'
    }

  ];
  public shopByPrice=[
    {
      image:'assets/images/shop-1.png'
    },
    {
      image:'assets/images/shop-2.png'
    },
    {
      image:'assets/images/shop-3.png'
    },
    {
      image:'assets/images/shop-4.png'
    }

  ];
  public shopByOccasion=[
    {
      image:'assets/images/shop-1.png'
    },
    {
      image:'assets/images/shop-2.png'
    },
    {
      image:'assets/images/shop-3.png'
    },
    {
      image:'assets/images/shop-4.png'
    }

  ];
  public featured=[
    {
      image:'assets/images/pre_packed.jpg'
    },
    {
      image:'assets/images/toss_up.jpg'
    }

];
  // Collection banner 300 x 300
  public collections = [{
    image: 'assets/images/shopping3.jpg',
    save: 'save 50%',
    title: 'men'
  }, {
    image: 'assets/images/shopping3.jpg',
    save: 'save 50%',
    title: 'women'
  },
  {
    image: 'assets/images/shopping3.jpg',
    save: 'save 50%',
    title: 'men'
  }, {
    image: 'assets/images/shopping3.jpg',
    save: 'save 50%',
    title: 'women'
  }
];

  // Blog
  public blog = [{
    image: 'assets/images/blog/1.jpg',
    date: '25 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }, {
    image: 'assets/images/blog/2.jpg',
    date: '26 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }, {
    image: 'assets/images/blog/3.jpg',
    date: '27 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }, {
    image: 'assets/images/blog/4.jpg',
    date: '28 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }];

  // Logo
  public logo = [{
    image: 'assets/images/logos/1.png',
  }, {
    image: 'assets/images/logos/2.png',
  }, {
    image: 'assets/images/logos/3.png',
  }, {
    image: 'assets/images/logos/4.png',
  }, {
    image: 'assets/images/logos/5.png',
  }, {
    image: 'assets/images/logos/6.png',
  }, {
    image: 'assets/images/logos/7.png',
  }, {
    image: 'assets/images/logos/8.png',
  }];

  getHome(){
    this.webService.get(Url.API.home.getHome).subscribe((response) => {
      this.homepageObj = response.homePageDetails;
      this.festive = this.homepageObj.tabSection.festiveOffers.productItems;
      this.newA = this.homepageObj.tabSection.newArrival.productItems;
      this.bestseller = this.homepageObj.tabSection.bestSeller.productItems;
      this.sliders=this.homepageObj.carouselSection.sliderItems;
      this.services= this.homepageObj.whyShopSection.sectionItems;
      if (response.status && response.status.code === 200) {
        //this.categories= 

      }
    }, () => {
      
    });

  }
  ngOnInit(): void {
  }

  // Product Tab collection
  getCollectionProducts(collection) {
    return this.products.filter((item) => {
      if (item.collection.find(i => i === collection)) {
        return item
      }
    })
  }
  
}
