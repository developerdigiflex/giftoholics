export interface Category{
    id:string;
    name:string;
    displayName:string;
    displayOrder:number;
}

export interface SubCategory{
    id:string;
    name:string;
    displayName:string;
    displayOrder:number;
    categoryId:string;
    category:Category;
}
export interface ProductItems{
    id:string;
    articleId:string;// 4 digit max
    categories:Category[];
    subCategories:SubCategory[];
    productTitle:string;
    productName:string;
    productDescription:string; //html format
    discountPercentage:number;
    shippingPrice:number;
    sellingPrice:number;
    vendorId:string;// static
    disclaimer:string; //html
    displayImage:string;
    productImages:string[];
    minQuantity:number;
    maxQuantity:number;
    availiableQuantity:number;
    customerInputRequired:boolean; 
    customerInputMessage:string; 
    tags:string;
    productRank:number;
    enlisted:boolean;
    canShipGlobally:boolean;
    productInternalNotes:string;
    ratings:number;
    newArrival:boolean;
    festiveOffer:boolean;
    bestSeller:boolean;
    deleted:boolean;


}

export interface FestiveOffers{
    tabName:string;
    productItems:ProductItems[];
}

export interface NewArrival{
    tabName:string;
    productItems:ProductItems[];
}

export interface BestSeller{
    tabName:string;
    productItems:ProductItems[];
}

export interface TabSection{
    festiveOffers:FestiveOffers;
    newArrival:NewArrival;
    bestSeller:BestSeller;
}

export interface SliderItems{
    redirectUrl:string;
    imageUrl:string;
}

export interface CarouselSection{
    topDisplayText:string;
    sliderItems:SliderItems[];
}

export interface SectionItems{
    displayText:string;
    displayImg:string;
    displaySubText:string;
}

export interface WhyShopSection{
    displayText:string;
    sectionItems:SectionItems[];
}

export class HomePageResponse{
    tabSection:TabSection;
    carouselSection:CarouselSection;
    whyShopSection:WhyShopSection;
}