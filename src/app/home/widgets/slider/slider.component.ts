import { Component, OnInit, Input } from '@angular/core';
import { HomeSlider } from '../../../shared/data/slider';
import { Router } from '@angular/router';
@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  
  @Input() sliders: any[];
  @Input() class: string;
  @Input() textClass: string;
  @Input() category: string;
  @Input() buttonText: string;
  @Input() buttonClass: string;

  constructor(private router: Router) { }

  ngOnInit(): void {

  }

  changeRoute(path){
    this.router.navigate(['../../shop/collection/left/sidebar?category=c1&c=5f538638320f5a338bf60b83']); 
  }
  
  public HomeSliderConfig: any = HomeSlider;

}
